<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/login', 'CustomLoginController@showLoginForm')->name('login.get');
Route::post('/login', 'CustomLoginController@login')->name('login.post');
Route::post('/logout', 'CustomLoginController@logout')->name('logout.post');

Route::get('/register', 'RegisterController@showRegisterForm')->name('register.get');
Route::post('/register', 'RegisterController@register')->name('register.post');
