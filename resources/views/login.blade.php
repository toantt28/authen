@extends('layouts.template')


@section('container')
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6 mt-5">
            <h3 class="title">Login</h3>

            @if ($errors->any())
                <ul class="text-danger">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            @endif

            @if (Session::has('message'))
                <div class="alert alert-warning">{{ Session::get('message') }}</div>
            @endif
            <form action="{{route('login.post')}}" method="POST">
                @csrf
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" id="email" aria-describedby="emailHelp" name="email" placeholder="Nhập Email đi ở dưới là giỡn thoi">
                    <small id="emailHelp" class="form-text text-muted">Điền vào là mất mail </small>
                </div>
                <div class="form-group">
                    <label for="password">Mat Khau</label>
                    <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                </div>
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="remember" id="remember">
                    <label class="form-check-label" for="remember">Remember me</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
@endsection