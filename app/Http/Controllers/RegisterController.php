<?php

namespace App\Http\Controllers;

use App\CustomUser;
use App\Http\Requests\UserRegisterRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function showRegisterForm()
    {
        return view('register');
    }

    public function register(UserRegisterRequest $request)
    {
        $request->validated();

        $user = CustomUser::create([
            'name'  => $request->get('name'),
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
        ]);

        Auth::guard()->login($user);
        return redirect('/');
    }
}
