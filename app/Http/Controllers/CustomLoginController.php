<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserLoginRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CustomLoginController extends Controller
{
    //
    public function showLoginForm()
    {
        return view('login');
    }

    public function login(UserLoginRequest $request)
    {
        $request->validated();

        $credentials = $request->only('email', 'password');
        if (Auth::attempt($credentials)) {
            return \redirect('/');
        } else {
            Session::flash('message', 'Sai ten dang nhap hoac mat khau');
            return Redirect::back();
        }
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return Redirect::back ();
    }
}
