<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:custom_users',
            'name' => 'required|string|max:50',
            'password' => 'required|confirmed|min:6'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email la bat buoc!',
            'email.unique' => 'Email da ton tai!',
            'name.required' => 'Ten la bat buoc!',
            'password.required' => 'Password la bat buoc',
            'password.confirmed' => 'Password khong khop',
        ];
    }
}
